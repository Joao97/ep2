import java.util.ArrayList;

public class Pokedex {

	private ArrayList<Pokemon> pokemons;
	private ArrayList<Treinador> treinadores;
	private LerCSV lerCsv;
	
	public Pokedex() {
		this.lerCsv = new LerCSV(pokemons);
		this.pokemons = new ArrayList<Pokemon>();
		this.treinadores = new ArrayList<Treinador>();
	
	}
	
	public void pesquisaPokemon(String nome, Pokemon pokemon) {
		for(int i = 0; i < pokemons.size(); i++) {
			if(nome.equals(pokemons.get(i).getNome())) {
				pokemon.setNome(pokemons.get(i).getNome());
	            pokemon.setId(pokemons.get(i).getId());
	            pokemon.setTipo1(pokemons.get(i).getTipo1());
	            pokemon.setTipo2(pokemons.get(i).getTipo2());
	            pokemon.setPontosDeVida(pokemons.get(i).getPontosDeVida());
	            pokemon.setAtaque(pokemons.get(i).getAtaque());
	            pokemon.setDefesa(pokemons.get(i).getDefesa());
	            pokemon.setAtaqueEspecial(pokemons.get(i).getAtaqueEspecial());
	            pokemon.setDefesaEspecial(pokemons.get(i).getDefesaEspecial());
	            pokemon.setVelocidade(pokemons.get(i).getVelocidade());
	            pokemon.setGeracao(pokemons.get(i).getGeracao());
	            pokemon.setLendario(pokemons.get(i).isLendario());
	            pokemon.setAltura((int)pokemons.get(i).getAltura());
	            pokemon.setPeso((int)pokemons.get(i).getPeso());
	            pokemon.setHabilidade1(pokemons.get(i).getHabilidade1());
	            pokemon.setHabilidade2(pokemons.get(i).getHabilidade2());
	            pokemon.setHabilidade3(pokemons.get(i).getHabilidade3());
			}
		}

	}
	
	public void pesquisaTipoPokemon(String tipo) {
		
	}
	
	public void pesquisaPokemon(int id, Pokemon pokemon) {
		for(int i = 0; i < pokemons.size(); i++) {
			if(id == pokemons.get(i).getId()) {
				pokemon.setNome(pokemons.get(i).getNome());
	            pokemon.setId(pokemons.get(i).getId());
	            pokemon.setTipo1(pokemons.get(i).getTipo1());
	            pokemon.setTipo2(pokemons.get(i).getTipo2());
	            pokemon.setPontosDeVida(pokemons.get(i).getPontosDeVida());
	            pokemon.setAtaque(pokemons.get(i).getAtaque());
	            pokemon.setDefesa(pokemons.get(i).getDefesa());
	            pokemon.setAtaqueEspecial(pokemons.get(i).getAtaqueEspecial());
	            pokemon.setDefesaEspecial(pokemons.get(i).getDefesaEspecial());
	            pokemon.setVelocidade(pokemons.get(i).getVelocidade());
	            pokemon.setGeracao(pokemons.get(i).getGeracao());
	            pokemon.setLendario(pokemons.get(i).isLendario());
	            pokemon.setAltura((int)pokemons.get(i).getAltura());
	            pokemon.setPeso((int)pokemons.get(i).getPeso());
	            pokemon.setHabilidade1(pokemons.get(i).getHabilidade1());
	            pokemon.setHabilidade2(pokemons.get(i).getHabilidade2());
	            pokemon.setHabilidade3(pokemons.get(i).getHabilidade3());
			}
		}

	}
	
	public boolean pesquisarTreinador(String nomeDeUsuario) {
		return true;
	}

	public void cadastrarTreinador(String nome, int idade, String sexo, String nomeDeUsuario) {
		
	}
	
	public void capturarPokemon(String nomeDeUsuario) {
		
	}
	
	public void obterPerfilTreinador(String nomeDeUsuario) {
		
	}
	
}
