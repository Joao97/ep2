import java.util.ArrayList;

public class Treinador extends Pessoa {
	
	private String nomeDeUsuario;
	private ArrayList<Pokemon> pokemons;
	
	public Treinador() {

		this.pokemons = new ArrayList<Pokemon>();

	}

	public ArrayList<Pokemon> getPokemons() {
		return pokemons;
	}

	public void setPokemons(Pokemon pokemons) {
		this.pokemons.add(pokemons);
	}

	public String getNomeDeUsuario() {
		return nomeDeUsuario;
	}

	public void setNomeDeUsuario(String nomeDeUsuario) {
		this.nomeDeUsuario = nomeDeUsuario;
	}	


}
