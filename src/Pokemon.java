
public class Pokemon {
	
	private String nome;
	private String tipo1;
	private String tipo2;
	private String habilidade1;
	private String habilidade2;
	private String habilidade3;
	private int id;
	private int ataque;
	private int defesa;
	private int pontosDeVida;
	private int ataqueEspecial;
	private int defesaEspecial;
	private int geracao;
	private int velocidade;
	private float peso;
	private float altura;
	private boolean lendario;

	public Pokemon() {

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo1() {
		return tipo1;
	}

	public void setTipo1(String tipo1) {
		this.tipo1 = tipo1;
	}

	public String getTipo2() {
		return tipo2;
	}

	public void setTipo2(String tipo2) {
		this.tipo2 = tipo2;
	}

	public String getHabilidade1() {
		return habilidade1;
	}

	public void setHabilidade1(String habilidade1) {
		this.habilidade1 = habilidade1;
	}

	public String getHabilidade2() {
		return habilidade2;
	}

	public void setHabilidade2(String habilidade2) {
		this.habilidade2 = habilidade2;
	}

	public String getHabilidade3() {
		return habilidade3;
	}

	public void setHabilidade3(String habilidade3) {
		this.habilidade3 = habilidade3;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAtaque() {
		return ataque;
	}

	public void setAtaque(int ataque) {
		this.ataque = ataque;
	}

	public int getDefesa() {
		return defesa;
	}

	public void setDefesa(int defesa) {
		this.defesa = defesa;
	}

	public int getPontosDeVida() {
		return pontosDeVida;
	}

	public void setPontosDeVida(int pontosDeVida) {
		this.pontosDeVida = pontosDeVida;
	}

	public int getAtaqueEspecial() {
		return ataqueEspecial;
	}

	public void setAtaqueEspecial(int ataqueEspecial) {
		this.ataqueEspecial = ataqueEspecial;
	}

	public int getDefesaEspecial() {
		return defesaEspecial;
	}

	public void setDefesaEspecial(int defesaEspecial) {
		this.defesaEspecial = defesaEspecial;
	}

	public int getGeracao() {
		return geracao;
	}

	public void setGeracao(int geracao) {
		this.geracao = geracao;
	}

	public int getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(int velocidade) {
		this.velocidade = velocidade;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = (float) peso/10;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = (float) altura/10;
	}

	public boolean isLendario() {
		return lendario;
	}

	public void setLendario(boolean lendario) {
		this.lendario = lendario;
	}
	
	public void mostrarDados() {
		
	}

}
