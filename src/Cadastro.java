import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class Cadastro extends JInternalFrame {
	private JTextField tfNome;
	private JTextField tfNomeUsuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cadastro frame = new Cadastro(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Cadastro(Pokedex pokedex) {
		setTitle("Cadastro de treinadores");
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setForeground(Color.WHITE);
		setClosable(true);
		setBounds(100, 100, 450, 323);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 102));
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 12, 46, 14);
		panel.add(lblNome);
		
		JLabel lblIdade = new JLabel("Idade");
		lblIdade.setBounds(12, 69, 46, 14);
		panel.add(lblIdade);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(12, 126, 46, 14);
		panel.add(lblSexo);
		
		JLabel lblNomeDeUsurio = new JLabel("Nome de Usu\u00E1rio");
		lblNomeDeUsurio.setBounds(12, 183, 104, 14);
		panel.add(lblNomeDeUsurio);
		
		tfNome = new JTextField();
		tfNome.setBounds(22, 30, 184, 28);
		panel.add(tfNome);
		tfNome.setColumns(10);
		
		tfNomeUsuario = new JTextField();
		tfNomeUsuario.setBounds(22, 201, 122, 28);
		panel.add(tfNomeUsuario);
		tfNomeUsuario.setColumns(10);
		
		JComboBox cbSexo = new JComboBox();
		cbSexo.setModel(new DefaultComboBoxModel(new String[] {"Selecione", "Masculino", "Feminino"}));
		cbSexo.setBounds(22, 143, 111, 28);
		panel.add(cbSexo);
		
		JSpinner spIdade = new JSpinner();
		spIdade.setModel(new SpinnerNumberModel(10, 10, 130, 1));
		spIdade.setBounds(22, 86, 53, 28);
		panel.add(spIdade);
		
		JButton btnCadastrarTreinador = new JButton("Cadastrar treinador");
		btnCadastrarTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//pokedex.cadastrarTreinador(tfNome.getText(), (Integer) spIdade.getValue() , cbSexo.getSelectedItem().toString(), tfNomeUsuario.getText());
				JOptionPane.showMessageDialog(null, "Treinador cadastrado com sucesso!");
				try {
					setClosed(true);
				} catch (PropertyVetoException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnCadastrarTreinador.setBounds(280, 249, 142, 28);
		panel.add(btnCadastrarTreinador);
		
		

	}
}
