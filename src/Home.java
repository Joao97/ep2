import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.UIManager.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JDesktopPane;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Home extends JFrame {

	private JPanel contentPane;
	private final JPanel panel = new JPanel();
	private Pokedex pokedex = new Pokedex();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, you can set the GUI to another look and feel.
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home frame = new Home();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Home() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 637, 495);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		desktopPane.setBackground(new Color(153, 204, 204));
		desktopPane.setBounds(0, 25, 631, 441);
		contentPane.add(desktopPane);
		panel.setBorder(UIManager.getBorder("MenuBar.border"));
		panel.setBounds(0, 0, 631, 26);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(new Color(255, 255, 255));
		menuBar.setBounds(0, 0, 631, 26);
		panel.add(menuBar);
		
		JMenu mnTreinador = new JMenu("Treinador");
		menuBar.add(mnTreinador);
		
		JMenuItem mntmPesquisar = new JMenuItem("Pesquisar");
		mntmPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		mnTreinador.add(mntmPesquisar);
		
		JMenuItem mntmCadastrar = new JMenuItem("Cadastrar");
		mntmCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cadastro telaDeCadastro = new Cadastro(pokedex);
				desktopPane.add(telaDeCadastro);
				telaDeCadastro.setVisible(true);
			}
		});
		mnTreinador.add(mntmCadastrar);
		
		JMenuItem mntmAdicionarPokemon = new JMenuItem("Adicionar Pokemon");
		mnTreinador.add(mntmAdicionarPokemon);
		
		JMenu mnPokemons = new JMenu("Pokemons");
		menuBar.add(mnPokemons);
		
		JMenuItem mntmPesquisarPorNomeid = new JMenuItem("Pesquisar por nome/id");
		mnPokemons.add(mntmPesquisarPorNomeid);
		
		JMenuItem mntmPesquisarPorTipo = new JMenuItem("Pesquisar por tipo");
		mnPokemons.add(mntmPesquisarPorTipo);
	}
}
